# [React](https://reactjs.org/)

[![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg)](https://github.com/facebook/react/blob/master/LICENSE) [![npm version](https://img.shields.io/npm/v/react.svg?style=flat)](https://www.npmjs.com/package/react) [![Coverage Status](https://img.shields.io/coveralls/facebook/react/master.svg?style=flat)](https://coveralls.io/github/facebook/react?branch=master) [![CircleCI Status](https://circleci.com/gh/facebook/react.svg?style=shield&circle-token=:circle-token)](https://circleci.com/gh/facebook/react) [![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg)](https://reactjs.org/docs/how-to-contribute.html#your-first-pull-request)

---

Este proyecto ha sido creado con [Create React App](https://github.com/facebookincubator/create-react-app).

[Aquí](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md) puedes encontrar una guía de como utilizar React.

---

## Iniciar el proyecto

Para poder iniciar el proyecto debemos primero crear un clon en local del mismo, para eso haremos la siguiente instrucción:

~~~bash
mkidr ICEBIRD
cd ICEBIRD
git clone git@gitlab.com:icebird/front.git
~~~

> Es importante que esté dentro de la carpeta `ICEBIRD`.

----

Una vez clonado el repositorio accedemos y ejecutamos el siguiente comando:

~~~bash
yarn
~~~

> Si no dispones de yarn utiliza:
>  ~~~bash
>  npm i yarn -g
>  ~~~
> Este comando instala yarn de forma global

Esto nos generará, a partir del archivo `package.json`, la carpeta `node_modules` que contiene los repositorios necesarios para el proyecto.

----

<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

Antes de poder enceder el proyecto es importante encender la api de `Laravel`, [aquí](https://gitlab.com/icebird/back/blob/master/README.md) puedes ver la documentación de como ponerla en marcha

----

Ahora ya podemos arrancar la aplicación, para ello basta con escribir el siguiente comando:

~~~bash
yarn start
~~~

Si has realizado todos los pasos correctamente nos correrá la aplicación en `localhost:3000`

---

## Recursos utilizados

### Repositorios

- **axios**: Repositorio para realizar consultas ajax mediante la API de `Laravel`.
- **material-ui**: Repositorio que incluye diversos diseños de interfaz de usuario.
- **react-icons-kit**: Repositorio que incluye una recopilación de diferentes iconos, entre ellos `font awesome`, `material icons`, etc.
- **react-loading**: Este repositorio incluye animaciones de loading para los tiempos de carga.
- **react-select**: La etiqueta `select` de `HTML5` pero mejorada y adapta a `React`.
- **react-sidenav**: Un navegador lateral en `React`.
- **react-table**: Una tabla hecha con `React`.
- **react-router-dom**: Repositorio para definir rutas para la aplicación.

### Imagenes

- Pixabay
  - [Logo](https://pixabay.com/es/equipo-m%C3%ADstico-pokemon-pokemon-ir-1572484/)

---
