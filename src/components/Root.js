import querystring from 'querystring'
import React from 'react'
import { Route, Switch, Router } from 'react-router-dom'
import createBrowserHistory from 'history/createBrowserHistory'
import axios from 'axios'
import cookie from 'react-cookies'

import Admin from './Admin'
import LoginAdmin from './LoginAdmin/LoginAdmin'
import Error from './Admin/Error/Error'

const history = createBrowserHistory()

export default class Root extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      redirect: false,
      mail: '',
      pass: '',
      loaded: false,
      loading: false,
      error: false,
      error_message: '',
    }
    this.login = this.login.bind(this)
  }

  login(mail, pass) {
    this.setState({ mail, pass, loading: true, error: false })
    const data = querystring.stringify({
      grant_type: 'password',
      client_id: 2,
      client_secret: 'oDGFu4EUBXkTIIxzk1ujXxEOKMtPUNTrX0t0glMN',
      username: mail,
      password: pass,
    })
    axios.post('http://localhost/ICEBIRD/back/public/oauth/token', data)
      .then((token) => {
        const config = {
          headers: {'Authorization': 'Bearer ' + token.data.access_token},
        }
        axios.get('http://localhost/ICEBIRD/back/public/api/rol', config)
          .then((rol) => {
            if (rol.data) {
              cookie.save('token', token.data.access_token, { path: '/' })
              this.setState({ redirect: true, loading: false })
            }
          })
          .catch(() => {
            this.setState({ loading: false, error: true, error_message: 'No tienes acceso!' })
          })
      })
      .catch(error => {
        this.setState({ loading: false, error: true, error_message: error.response.data.message })
      })
  }

  render() {
    const LoginAdminProps = (props) => {
      return (
        <LoginAdmin
          login={this.login}
          redirect={this.state.redirect}
          email={this.state.mail}
          pass={this.state.pass}
          loaded={this.state.loaded}
          loading={this.state.loading}
          error={this.state.error}
          error_message={this.state.error_message}
          {...props}
        />
      )
    }
    return (
      <Router history={history}>
        <Switch>
          <Route exact path="/" component={LoginAdminProps} />
          <Route path="/admin/dashboard" component={Admin} />
          <Route component={Error}/>
        </Switch>
      </Router>
    )
  }
}
