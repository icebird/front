import React, { Component } from 'react'
import SvgIcon from 'react-icons-kit'

import { ic_delete } from 'react-icons-kit/md/ic_delete'
import {truck} from 'react-icons-kit/fa/truck'
import {checkCircle} from 'react-icons-kit/fa/checkCircle'

export default class ActionsPedidos extends Component {

  render() {
    return (
      <div className='btn-group'>
        <button title='Enviado' type='button' onClick={this.props.estadoEnviado.bind(this, this.props.item)} className='btn btn-xs btn-info' aria-hidden='true'>
          <SvgIcon size={20} icon={truck}/>
        </button>
        <button title='Entregado' type='button' onClick={this.props.estadoEntregado.bind(this, this.props.item)} className='btn btn-xs btn-success' aria-hidden='true'>
          <SvgIcon size={20} icon={checkCircle}/>
        </button>
        <button title='Cancelar' type='button' onClick={this.props.estadoCancelado.bind(this, this.props.item)} className='btn btn-xs btn-danger' aria-hidden='true'>
          <SvgIcon size={20} icon={ic_delete}/>
        </button>
      </div>
    )
  }
}
