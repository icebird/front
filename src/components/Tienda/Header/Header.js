import React, { Component } from 'react'

import './Header.css'
import logo from '../../../img/logo.png'

export default class Header extends Component {
  render() {
    return (
      <div className="header row">
        <div className='logo col-md-2'>
          <img alt='logo' src={logo} height='100px' width='100px'/>
        </div>
      </div>
    )
  }
}
