import React from 'react'
import { withRR4, Nav, NavText, NavIcon } from 'react-sidenav'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import axios from 'axios'
import SvgIcon from 'react-icons-kit'

import {ic_home} from 'react-icons-kit/md/ic_home'
import {ic_business_center} from 'react-icons-kit/md/ic_business_center'

import { routes } from '../../constants/routesStore'
import Header from './Header/Header'

const SideNav = withRR4()

export default class Tienda extends React.Component {
  state = {
    categorias: [],
  }
  componentDidMount(){
    axios.get('http://localhost/ICEBIRD/back/public/api/category')
      .then(res => {
        const categorias = res.data.data
        let i
        for(i = 0; i < categorias.length; i++){
          categorias[i].value = categorias[i]['id']
          categorias[i].label = categorias[i]['nombre']
          delete categorias[i].id
          delete categorias[i].nombre
        }
        this.setState({ categorias })
      })
      .catch(error => {
        console.log(error.message)
      })
  }
  render() {
    console.log(this.state.categorias)
    const navs = []
    for (let i = 0; i < this.state.categorias.length; i++) {
      navs.push({
        id: 'tienda/categoria/' + this.state.categorias[i].value,
        name: this.state.categorias[i].label,
      })
    }
    console.log(navs)
    return (
      <div className='container-fluid'>
        <Header/>
        <Router>
          <div className='sidebar row'>
            <div className='sidenav col-md-2'>
              <SideNav defaultSelected='/' highlightBgColor='#384e64' highlightColor='white'>
                <Nav id='tienda/inicio'>
                  <NavIcon><SvgIcon size={20} icon={ic_home}/></NavIcon>
                  <NavText>Inicio</NavText>
                </Nav>
                {navs.map((nav, index) => (
                  <Nav key={index} id={nav.id}>
                    <NavIcon><SvgIcon size={20} icon={ic_business_center}/></NavIcon>
                    <NavText>{nav.name}</NavText>
                  </Nav>
                ))}
              </SideNav>
            </div>
            <div className='content col-md-11'>
              {routes.map((route, index) => (
                <Route
                  key={index}
                  path={route.path}
                  exact={route.exact}
                  component={route.main}
                />
              ))}
            </div>
          </div>
        </Router>
      </div>
    )
  }
}
