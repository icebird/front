import React, { Component } from 'react'
import axios from 'axios'
import ReactLoading from 'react-loading'
import { confirmAlert } from 'react-confirm-alert'
import Alert from 'react-s-alert'
import cookie from 'react-cookies'
import { Redirect } from 'react-router-dom'

import 'react-confirm-alert/src/react-confirm-alert.css'

import Table from '../../Table'
import ActionsPedidos from '../../ActionsPedidos/ActionsPedidos'


export default class Pedidos extends Component {
  constructor() {
    super()
    this.state = {
      pedidos: [],
      loaded: false,
      error: false,
      error_message: '',
      reload: false,
    }
  }

  estadoCancelado(item) {
    confirmAlert({
      title: 'Cancelado',
      message: '¿Quieres marcar el producto cómo cancelado?',
      buttons: [
        {
          label: 'Sí',
          onClick: () => this.cancelado(item),
        },
        {
          label: 'No',
          onClick: () => alert('No'),
        },
      ],
    })
  }

  estadoEnviado(item) {
    console.log(item)
    confirmAlert({
      title: 'Enviado',
      message: '¿Quieres marcar el producto cómo enviado?',
      buttons: [
        {
          label: 'Sí',
          onClick: () => this.enviado(item),
        },
        {
          label: 'No',
          onClick: () => alert('No'),
        },
      ],
    })
  }

  estadoEntregado(item) {
    confirmAlert({
      title: 'Entregado',
      message: '¿Quieres marcar el producto cómo entregado?',
      buttons: [
        {
          label: 'Sí',
          onClick: () => this.entregado(item),
        },
        {
          label: 'No',
          onClick: () => alert('No'),
        },
      ],
    })
  }

  cancelado(item) {
    const config = {
      headers: {'Authorization': 'Bearer ' + cookie.load('token')},
    }
    axios.put('http://localhost/ICEBIRD/back/public/api/cancelled/' + item.id, config)
      .then( () => {
        Alert.error('Pedido marcado cómo cancelado', {
          position: 'top-right',
          effect: 'slide',
          html: true,
        })
        this.setState({ reload: true })
      })
  }

  entregado(item) {
    const config = {
      headers: {'Authorization': 'Bearer ' + cookie.load('token')},
    }
    axios.put('http://localhost/ICEBIRD/back/public/api/delivered/' + item.id, config)
      .then( () => {
        Alert.success('Pedido marcado cómo entregado', {
          position: 'top-right',
          effect: 'slide',
          html: true,
        })
        this.setState({ reload: true })
      })
  }

  enviado(item) {
    const config = {
      headers: {'Authorization': 'Bearer ' + cookie.load('token')},
    }
    axios.put('http://localhost/ICEBIRD/back/public/api/send/' + item.id, config)
      .then( () => {
        Alert.info('Pedido marcado cómo enviado', {
          position: 'top-right',
          effect: 'slide',
          html: true,
        })
        this.setState({ reload: true })
      })
  }

  listarPedidos(){
    const config = {
      headers: {'Authorization': 'Bearer ' + cookie.load('token')},
    }
    axios.get('http://localhost/ICEBIRD/back/public/api/orders', config)
      .then(res => {
        const pedidos = res.data
        this.setState({ pedidos: pedidos, loaded: true })
      })
      .catch(error => {
        console.log(error.message)
        this.setState({ error: true, error_message: error.message, loaded: true })
      })
  }
  componentDidMount() {
    this.listarPedidos()
  }

  render() {
    const noDataText = this.state.error ? this.state.error_message : 'No hay pedidos!'
    const link = 'generos/editar/'
    const columns = [
      {
        Header: 'ID',
        accessor: 'id',
      },
      {
        Header: 'Pedido',
        accessor: 'pedido',
      },
      {
        Header: 'ID Usuario',
        accessor: 'idUsuario',
      },
      {
        Header: 'Estado',
        accessor: 'estado',
      },
      {
        Header: 'Acciones',
        id: 'actions',
        accessor:(item) => (
          <ActionsPedidos
            item={item}
            linkEdit={link + item.id}
            estadoEnviado={this.estadoEnviado.bind(this)}
            estadoEntregado={this.estadoEntregado.bind(this)}
            estadoCancelado={this.estadoCancelado.bind(this)}
          />
        ),

      },
    ]

    if (this.state.reload) {
      return (
        <Redirect to='/admin/dashboard'/>
      )
    }

    if (this.state.loaded === false) {
      return (
        <div className="d-flex justify-content-center mt-5">
          <ReactLoading type="spokes" color="#808080"/>
        </div>
      )
    }

    return (
      <Table
        data={this.state.pedidos}
        noDataText={noDataText}
        columns={columns}
      />
    )
  }
}
