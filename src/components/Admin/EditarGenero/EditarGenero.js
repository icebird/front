import React, { Component } from 'react'
import { Card, CardTitle, CardText } from 'material-ui/Card'
import axios from 'axios'
import { Redirect } from 'react-router'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import Alert from 'react-s-alert'
import cookie from 'react-cookies'
import 'react-select/dist/react-select.css'

export default class EditarGenero extends Component {
  constructor() {
    super()
    this.state = {
      redirect: false,
      nombre: '',
      token: cookie.load('token'),
    }
  }

  componentDidMount() {
    const config = {
      headers: {'Authorization': 'Bearer ' + cookie.load('token')},
    }
    axios.get('http://localhost/ICEBIRD/back/public/api/gender/' + this.props.match.params.id, config)
      .then( (res) => {
        const genero = res.data
        this.setState({
          nombre: genero.data.nombre,
        })

      })
  }

  handleTextChange = (input) => {
    this.setState({ [input.target.id]: input.target.value })
  }

  handleNumChange = (input, valueAsNumber, valueAsString) => {
    this.setState({ [valueAsString.id]: valueAsNumber })
  }

  handleSubmit() {
    const config = {
      headers: {'Authorization': 'Bearer ' + cookie.load('token')},
    }
    const { nombre } = this.state
    axios.put('http://localhost/ICEBIRD/back/public/api/gender/' + this.props.match.params.id, {
      nombre: nombre,
    },
    config)
      .then((res) => {
        if (res.data === 'Ha ocurrido un error al validar los datos del género') {
          Alert.error(res.data, {
            position: 'top-right',
            effect: 'slide',
          })
        } else {
          this.setState({ redirect: true })
          Alert.success('Género actualizado satisfactoriamente!', {
            position: 'top-right',
            effect: 'slide',
          })
        }
      })
      .catch(error => {
        Alert.error(error.message, {
          position: 'top-right',
          effect: 'slide',
        })
      })
  }

  render() {
    const { redirect } = this.state
    if (redirect) {
      return(
        <Redirect to='/admin/generos'/>
      )
    }
    return (
      <MuiThemeProvider>
        <Card>
          <CardTitle title="Actualizar Género" subtitle="Formulario de actualización de género" />
          <CardText style={{height: '100%'}}>
            <form>
              <div className='form-group'>
                <label htmlFor='nombre'>Nombre:</label>
                <input id='nombre' value={this.state.nombre} onChange={this.handleTextChange} className='form-control' type='string'/>
              </div>
              <button type='button' onClick={this.handleSubmit.bind(this)} className='btn btn-primary'>Enviar</button>
            </form>
          </CardText>
        </Card>
      </MuiThemeProvider>
    )
  }
}

