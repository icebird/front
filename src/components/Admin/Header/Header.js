import React, { Component } from 'react'
import SvgIcon from 'react-icons-kit'

import {ic_dvr} from 'react-icons-kit/md/ic_dvr'
import {ic_reorder} from 'react-icons-kit/md/ic_reorder'

import './Header.css'

export default class Header extends Component {
  render() {
    return (
      <div className="header row">
        <div className={this.props.classHeader}>
          <a href="/"><SvgIcon className='logo-img' size={30} icon={ic_dvr}/></a>
          <h1>IceBird Admin</h1>
        </div>
        <button className='reorder' type='button' onClick={this.props.changeStyle}>
          <SvgIcon size={20} icon={ic_reorder}/>
        </button>
        <button className='reorder' type='button' onClick={this.props.logout}>
          Logout
        </button>
        <div>
        </div>
      </div>
    )
  }
}
