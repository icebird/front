import React, { Component } from 'react'
import cookie from 'react-cookies'
import { Redirect } from 'react-router-dom'


import './Error.css'

export default class Productos extends Component {
  render() {
    if (cookie.load('token')) {
      return (
        <Redirect to='/admin/dashboard'/>
      )
    } else {
      return (
        <Redirect to='/'/>
      )
    }
  }
}
