import React, { Component } from 'react'
import axios from 'axios'
import ReactLoading from 'react-loading'
import cookie from 'react-cookies'

import Table from '../../Table'

export default class Usuarios extends Component {
  constructor() {
    super()
    this.state = {
      usuarios: [],
      loaded: false,
      error: false,
      error_message: '',
    }
  }

  componentDidMount() {
    const config = {
      headers: {'Authorization': 'Bearer ' + cookie.load('token')},
    }
    axios.get('http://localhost/ICEBIRD/back/public/api/users', config)
      .then(res => {
        const usuarios = res.data.data
        this.setState({ usuarios, loaded: true })
      })
      .catch(error => {
        this.setState({ error: true, error_message: error.message, loaded: true })
      })
  }

  render() {
    const noDataText = this.state.error ? this.state.error_message : 'No hay usuarios!'
    const columns = [
      {
        Header: 'ID',
        accessor: 'id',
      },
      {
        Header: 'Usuario',
        accessor: 'user',
      },
      {
        Header: 'Email',
        accessor: 'email',
      },
      {
        Header: 'Acciones',
        accessor: 'acciones',
      },
    ]

    if (this.state.loaded === false) {
      return (
        <div className="d-flex justify-content-center mt-5">
          <ReactLoading type="spokes" color="#808080"/>
        </div>
      )
    }
    return (
      <Table
        data={this.state.usuarios}
        noDataText={noDataText}
        columns={columns}
      />
    )
  }
}
