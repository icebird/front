import React, { Component } from 'react'
import { Card, CardTitle, CardText } from 'material-ui/Card'
import axios from 'axios'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import { Redirect } from 'react-router'
import Alert from 'react-s-alert'
import cookie from 'react-cookies'

import 'react-select/dist/react-select.css'

export default class NuevoGenero extends Component {
  state = {
    redirect: false,
    nombre: '',
  }

  handleTextChange = (input) => {
    this.setState({ [input.target.id]: input.target.value })
  }

  handleSubmit() {
    const { nombre } = this.state
    const config = {
      headers: {'Authorization': 'Bearer ' + cookie.load('token')},
    }
    axios.post('http://localhost/ICEBIRD/back/public/api/gender', {
      nombre: nombre,
    },
    config)
      .then((res) => {
        if (res.data === 'Ha ocurrido un error al validar los datos del género') {
          Alert.error(res.data, {
            position: 'top-right',
            effect: 'slide',
          })
        } else {
          this.setState({ redirect: true })
          Alert.success('Género creado satisfactoriamente!', {
            position: 'top-right',
            effect: 'slide',
          })
        }
      })
      .catch(error => {
        Alert.error(error.message, {
          position: 'top-right',
          effect: 'slide',
        })
      })
  }

  render() {
    const { redirect } = this.state
    if (redirect) {
      return(
        <Redirect to='/admin/generos'/>
      )
    }
    return (
      <MuiThemeProvider>
        <Card>
          <CardTitle title="Nuevo género" subtitle="Formulario de creación de un nuevo género" />
          <CardText style={{height: '100%'}}>
            <form>
              <div className='form-group'>
                <label htmlFor='nombre'>Nombre:</label>
                <input id='nombre' onChange={this.handleTextChange} className='form-control' type='string'/>
              </div>
              <button type='button' onClick={this.handleSubmit.bind(this)} className='btn btn-primary'>Enviar</button>
            </form>
          </CardText>
        </Card>
      </MuiThemeProvider>
    )
  }
}
