import React, { Component } from 'react'
import { Card, CardTitle, CardText } from 'material-ui/Card'
import axios from 'axios'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import Select from 'react-select'
import NumericInput from 'react-numeric-input'
import { Redirect } from 'react-router'
import Alert from 'react-s-alert'
import cookie from 'react-cookies'
import 'react-select/dist/react-select.css'

export default class NuevoProducto extends Component {
  state = {
    redirect: false,
    categorias: [],
    generos: [],
    nombre: '',
    stock: 0,
    pvp: 0,
    descuento: 0,
    precio: 0,
    idGenero: 0,
    idCategoria: 0,
    descripcion: '',
    marca: '',
    distribuidor: '',
    imagen: null,
    token: cookie.load('token'),
  }

  componentDidMount() {
    const config = {
      headers: {'Authorization': 'Bearer ' + this.state.token},
    }

    axios.get('http://localhost/ICEBIRD/back/public/api/category', config)
      .then(res => {
        const categorias = res.data.data
        let i
        for(i = 0; i < categorias.length; i++){
          categorias[i].value = categorias[i]['id']
          categorias[i].label = categorias[i]['nombre']
          delete categorias[i].id
          delete categorias[i].nombre
        }
        this.setState({ categorias })
      })
      .catch(error => {
        console.log(error.message)
      })

    axios.get('http://localhost/ICEBIRD/back/public/api/gender', config)
      .then(res => {
        const generos = res.data.data
        let i
        for(i = 0; i < generos.length; i++){
          generos[i].value = generos[i]['id']
          generos[i].label = generos[i]['nombre']
          delete generos[i].id
          delete generos[i].nombre
        }
        this.setState({ generos })
      })
      .catch(error => {
        console.log(error.message)
      })
  }

  fileChangedHandler = (event) => {
    const reader = new FileReader()
    reader.onload = (e) => {
      this.setState({
        imagen: e.target.result,
      })
    }
    reader.readAsDataURL(event.target.files[0])
  }

  handleCategory = (idCategoria) => {
    if (idCategoria === null) {
      idCategoria = {
        value: 0,
        label: '',
      }
    }
    this.setState({ idCategoria: idCategoria.value })
  }

  handleGender = (idGenero) => {
    if (idGenero === null) {
      idGenero = {
        value: 0,
        label: '',
      }
    }
    this.setState({ idGenero: idGenero.value })
  }

  handleTextChange = (input) => {
    this.setState({ [input.target.id]: input.target.value })
  }

  handleNumChange = (input, valueAsNumber, valueAsString) => {
    this.setState({ [valueAsString.id]: valueAsNumber })
  }

  handleSubmit() {
    const config = {
      headers: {'Authorization': 'Bearer ' + cookie.load('token')},
    }
    const { nombre, stock, pvp, descuento, precio, idGenero, idCategoria, descripcion, marca, distribuidor, imagen } = this.state
    axios.post('http://localhost/ICEBIRD/back/public/api/products', {
      nombre: nombre,
      stock: stock,
      pvp: pvp,
      descuento: descuento,
      precio: precio,
      idGenero: idGenero,
      idCategoria: idCategoria,
      descripcion: descripcion,
      marca: marca,
      distribuidor: distribuidor,
    },
    config)
      .then((res) => {
        if (res.data === 'Ha ocurrido un error al validar los datos del producto') {
          Alert.error(res.data, {
            position: 'top-right',
            effect: 'slide',
          })
        } else {
          const id = res.data.id
          console.log(this.state.imagen)
          axios.post('http://localhost/ICEBIRD/back/public/api/media', {
            idProducto: id,
            file: imagen,
          },
          config)
            .then((res) => {
              console.log(res)
            })
            .catch((error) => {
              console.log(error)
            })
          this.setState({ redirect: true })
          Alert.success('Producto creado satisfactoriamente!', {
            position: 'top-right',
            effect: 'slide',
          })
        }
      })
      .catch(error => {
        Alert.error(error.message, {
          position: 'top-right',
          effect: 'slide',
        })
      })
  }

  render() {
    const { idCategoria, idGenero, redirect, token } = this.state
    if (!token) {
      return (
        <Redirect to='/admin'/>
      )
    }
    if (redirect) {
      return(
        <Redirect to='/admin/productos'/>
      )
    }
    return (
      <MuiThemeProvider>
        <Card>
          <CardTitle title="Nuevo producto" subtitle="Formulario de creación de un nuevo producto" />
          <CardText style={{height: '100%'}}>
            <form>
              <div className='form-group'>
                <label htmlFor='nombre'>Nombre:</label>
                <input id='nombre' onChange={this.handleTextChange} className='form-control' type='string'/>
              </div>
              <div className='form-group'>
                <label htmlFor='categoria'>Categoria:</label>
                <Select
                  id="categoria"
                  name="categoria"
                  value={idCategoria}
                  onChange={this.handleCategory}
                  options={this.state.categorias}
                />
              </div>
              <div className='form-group'>
                <label htmlFor='genero'>Género:</label>
                <Select
                  id="genero"
                  name="genero"
                  value={idGenero}
                  onChange={this.handleGender}
                  options={this.state.generos}
                />
              </div>
              <div className='form-group'>
                <label htmlFor='pvp'>PVP:</label>
                <NumericInput id='pvp' step={0.01} precision={2} onChange={this.handleNumChange} className='form-control' min={0}/>
              </div>
              <div className='form-group'>
                <label htmlFor='precio'>Precio:</label>
                <NumericInput id='precio' step={0.01} precision={2} onChange={this.handleNumChange} className='form-control' min={0}/>
              </div>
              <div className='form-group'>
                <label htmlFor='descuento'>Descuento:</label>
                <NumericInput id='descuento' onChange={this.handleNumChange} className='form-control' min={0}/>
              </div>
              <div className='form-group'>
                <label htmlFor='stock'>Stock:</label>
                <NumericInput id='stock' onChange={this.handleNumChange} className='form-control' min={0}/>
              </div>
              <div className='form-group'>
                <label htmlFor='distribuidor'>Distribuidor:</label>
                <input id='distribuidor' onChange={this.handleTextChange} className='form-control' type='string'/>
              </div>
              <div className='form-group'>
                <label htmlFor='marca'>Marca:</label>
                <input id='marca' onChange={this.handleTextChange} className='form-control' type='string'/>
              </div>
              <div className='form-group'>
                <label htmlFor='descripcion'>Descripcion:</label>
                <textarea id='descripcion' onChange={this.handleTextChange} className='form-control' rows='3'></textarea>
              </div>
              <div className='form-group'>
                <label htmlFor='descripcion'>Descripcion:</label>
                <input id='imagen' type="file" onChange={this.fileChangedHandler}/>
              </div>
              <button type='button' onClick={this.handleSubmit.bind(this)} className='btn btn-primary'>Enviar</button>
            </form>
          </CardText>
        </Card>
      </MuiThemeProvider>
    )
  }
}
