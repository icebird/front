import React from 'react'
import { withRR4, Nav, NavText, NavIcon } from 'react-sidenav'
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom'
import SvgIcon from 'react-icons-kit'
import Alert from 'react-s-alert'
import cookie from 'react-cookies'

import {ic_dashboard} from 'react-icons-kit/md/ic_dashboard'
import {ic_business_center} from 'react-icons-kit/md/ic_business_center'
import {ic_people} from 'react-icons-kit/md/ic_people'
import {ic_shopping_cart} from 'react-icons-kit/md/ic_shopping_cart'
import {ic_playlist_add} from 'react-icons-kit/md/ic_playlist_add'
import {tags} from 'react-icons-kit/fa/tags'
import {book} from 'react-icons-kit/fa/book'
import {ic_videogame_asset} from 'react-icons-kit/md/ic_videogame_asset'

import 'react-s-alert/dist/s-alert-default.css'
import 'react-s-alert/dist/s-alert-css-effects/slide.css'
import './Admin.css'

import { routes } from '../../constants/routes'
import Header from './Header'

const SideNav = withRR4()

export default class Admin extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      classHeader: 'logo col-md-2',
      classNav: 'sidenav col-md-2',
      classText: '',
      token: cookie.load('token'),
    }
    this.changeStyle = this.changeStyle.bind(this)
    this.logout = this.logout.bind(this)
  }

  changeStyle(){
    const cH = (this.state.classHeader === 'logo col-md-2') ? 'd-none' : 'logo col-md-2'
    const cN = (this.state.classNav === 'sidenav col-md-2') ? 'sidenav sidenav-xs' : 'sidenav col-md-2'
    const cT = (this.state.classText === '') ? 'd-none' : ''
    this.setState({classHeader: cH, classNav: cN, classText: cT})
  }

  logout(){
    cookie.remove('token', { path: '/' })
    window.location.reload()
  }

  render() {
    if (!this.state.token) {
      return (
        <Redirect to='/admin'/>
      )
    }
    return (
      <div className='container-fluid'>
        <Header classHeader={this.state.classHeader} changeStyle={this.changeStyle} logout={this.logout}/>
        <Router>
          <div className='sidebar row'>
            <div className={this.state.classNav}>
              <SideNav default='/admin' highlightBgColor='#384e64' highlightColor='white'>
                <Nav id='admin/dashboard'>
                  <NavIcon><SvgIcon size={20} icon={ic_dashboard}/></NavIcon>
                  <NavText className={this.state.classText}>Dashboard</NavText>
                </Nav>
                <Nav id='admin/productos'>
                  <NavIcon><SvgIcon size={20} icon={ic_business_center}/></NavIcon>
                  <NavText className={this.state.classText}>Productos</NavText>
                  <Nav id='nuevo'>
                    <NavText><SvgIcon size={20} icon={ic_playlist_add}/>&nbsp;<span className={this.state.classText}>Nuevo Producto</span></NavText>
                  </Nav>
                </Nav>
                <Nav id='admin/categorias'>
                  <NavIcon><SvgIcon size={20} icon={tags}/></NavIcon>
                  <NavText className={this.state.classText}>Categorias</NavText>
                  <Nav id='nuevo'>
                    <NavText><SvgIcon size={20} icon={ic_playlist_add}/>&nbsp;<span className={this.state.classText}>Nueva Categoría</span></NavText>
                  </Nav>
                </Nav>
                <Nav id='admin/generos'>
                  <NavIcon><SvgIcon size={20} icon={ic_videogame_asset}/></NavIcon>
                  <NavText className={this.state.classText}>Géneros</NavText>
                  <Nav id='nuevo'>
                    <NavText><SvgIcon size={20} icon={ic_playlist_add}/>&nbsp;<span className={this.state.classText}>Nuevo Género</span></NavText>
                  </Nav>
                </Nav>
                <Nav id='admin/usuarios'>
                  <NavIcon><SvgIcon size={20} icon={ic_people}/></NavIcon>
                  <NavText className={this.state.classText}>Usuarios</NavText>
                </Nav>
                <Nav id='admin/pedidos'>
                  <NavIcon><SvgIcon size={20} icon={ic_shopping_cart}/></NavIcon>
                  <NavText className={this.state.classText}>Pedidos</NavText>
                </Nav>
                <Nav id='admin/reservas'>
                  <NavIcon><SvgIcon size={20} icon={book}/></NavIcon>
                  <NavText className={this.state.classText}>Reservas</NavText>
                </Nav>
              </SideNav>
            </div>
            <div className='content col-md-11'>
              {routes.map((route, index) => (
                <Route
                  key={index}
                  path={route.path}
                  exact={route.exact}
                  component={route.main}
                />
              ))}
            </div>
          </div>
        </Router>
        <Alert stack={{limit: 3}} />
      </div>
    )
  }
}
