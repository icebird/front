import React, { Component } from 'react'
import axios from 'axios'
import ReactLoading from 'react-loading'
import { confirmAlert } from 'react-confirm-alert'
import { Redirect } from 'react-router-dom'
import Alert from 'react-s-alert'
import cookie from 'react-cookies'

import 'react-confirm-alert/src/react-confirm-alert.css'

import Table from '../../Table'
import Actions from '../../Actions/Actions'


export default class Categorias extends Component {
  constructor() {
    super()
    this.state = {
      categorias: [],
      loaded: false,
      error: false,
      error_message: '',
      reload: false,
    }
  }

  onRemoveClick(item) {
    confirmAlert({
      title: 'Borrar categoria',
      message: '¿Estás seguro de que quieres eliminar la categoria: ' + item.nombre + '?',
      buttons: [
        {
          label: 'Sí',
          onClick: () => this.borrarCategoria(item),
        },
        {
          label: 'No',
          onClick: () => alert('No'),
        },
      ],
    })
  }

  borrarCategoria(item) {
    const config = {
      headers: {'Authorization': 'Bearer ' + cookie.load('token')},
    }
    axios.delete('http://localhost/ICEBIRD/back/public/api/category/' + item.id, config)
      .then( () => {
        Alert.error('Se ha eliminado la categoria:</br><ul><li>' + item.nombre + '</li></ul>', {
          position: 'top-right',
          effect: 'slide',
          html: true,
        })
        this.setState({ reload: true })
      })
      .catch(error => {
        console.log(error.message)
      })
  }

  listarCategorias(){
    const config = {
      headers: {'Authorization': 'Bearer ' + cookie.load('token')},
    }
    axios.get('http://localhost/ICEBIRD/back/public/api/category', config)
      .then(res => {
        const categorias = res.data
        this.setState({ categorias: categorias.data, loaded: true })
      })
      .catch(error => {
        console.log(error.message)
        this.setState({ error: true, error_message: error.message, loaded: true })
      })
  }
  componentDidMount() {
    this.listarCategorias()
  }

  render() {
    const noDataText = this.state.error ? this.state.error_message : 'No hay categorias!'
    const link = 'categorias/editar/'
    const columns = [
      {
        Header: 'ID',
        accessor: 'id',
      },
      {
        Header: 'Nombre',
        accessor: 'nombre',
      },
      {
        Header: 'Acciones',
        id: 'actions',
        accessor:(item) => (
          <Actions
            item={item}
            linkEdit={link + item.id}
            onRemoveClick={this.onRemoveClick.bind(this)}
          />
        ),

      },
    ]

    if (this.state.reload) {
      return (
        <Redirect to='/admin/dashboard'/>
      )
    }

    if (this.state.loaded === false) {
      return (
        <div className="d-flex justify-content-center mt-5">
          <ReactLoading type="spokes" color="#808080"/>
        </div>
      )
    }

    return (
      <Table
        data={this.state.categorias}
        noDataText={noDataText}
        columns={columns}
      />
    )
  }
}
