import React, { Component } from 'react'
import axios from 'axios'
import ReactLoading from 'react-loading'
import { Redirect } from 'react-router-dom'
import { confirmAlert } from 'react-confirm-alert'
import Alert from 'react-s-alert'
import cookie from 'react-cookies'

import 'react-confirm-alert/src/react-confirm-alert.css'

import Table from '../../Table'
import Actions from '../../Actions/Actions'


export default class Productos extends Component {
  constructor() {
    super()
    this.state = {
      productos: [],
      loaded: false,
      error: false,
      error_message: '',
      token: cookie.load('token'),
      reload: false,
    }
  }

  onRemoveClick(item) {
    confirmAlert({
      title: 'Borrar producto',
      message: '¿Estás seguro de que quieres eliminar el producto: ' + item.nombre + '?',
      buttons: [
        {
          label: 'Sí',
          onClick: () => this.borrarProducto(item),
        },
        {
          label: 'No',
          onClick: () => alert('No'),
        },
      ],
    })
  }

  borrarProducto(item) {
    const config = {
      headers: {'Authorization': 'Bearer ' + this.state.token},
    }
    axios.delete('http://localhost/ICEBIRD/back/public/api/products/' + item.id, config)
      .then( () => {
        Alert.error('Se ha eliminado el producto:</br><ul><li>' + item.nombre + '</li></ul>', {
          position: 'top-right',
          effect: 'slide',
          html: true,
        })
        this.setState({ reload: true })
      })
      .catch(error => {
        console.log(error.message)
      })
  }

  listarProductos(){
    const config = {
      headers: {'Authorization': 'Bearer ' + this.state.token},
    }
    axios.get('http://localhost/ICEBIRD/back/public/api/products', config)
      .then(res => {
        const productos = res.data
        this.setState({ productos: productos.data, loaded: true })
      })
      .catch(error => {
        console.log(error.message)
        this.setState({ error: true, error_message: error.message, loaded: true })
      })
  }
  componentDidMount() {
    this.listarProductos()
  }

  render() {
    const noDataText = this.state.error ? this.state.error_message : 'No hay productos!'
    const link = 'productos/editar/'
    const columns = [
      {
        Header: 'ID',
        accessor: 'id',
      },
      {
        Header: 'Nombre',
        accessor: 'nombre',
      },
      {
        Header: 'Categoria',
        accessor: 'categoria',
      },
      {
        Header: 'Género',
        accessor: 'genero',
      },
      {
        Header: 'PVP',
        accessor: 'pvp',
      },
      {
        Header: 'Acciones',
        id: 'actions',
        accessor:(item) => (
          <Actions
            item={item}
            linkEdit={link + item.id}
            onRemoveClick={this.onRemoveClick.bind(this)}
          />
        ),

      },
    ]

    if (this.state.reload) {
      return (
        <Redirect to='/admin/dashboard'/>
      )
    }

    if (!this.state.token) {
      return (
        <Redirect to='/admin'/>
      )
    }

    if (this.state.loaded === false) {
      return (
        <div className="d-flex justify-content-center mt-5">
          <ReactLoading type="spokes" color="#808080"/>
        </div>
      )
    }

    return (
      <Table
        data={this.state.productos}
        noDataText={noDataText}
        columns={columns}
      />
    )
  }
}
