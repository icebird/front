import React from 'react'
import {Card, CardTitle, CardText} from 'material-ui/Card'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'


export default () => (
  <MuiThemeProvider>
    <Card>
      <CardTitle title="IceBird" subtitle="Panel de administración" />
      <CardText style={{height: '100%'}}>
        Bienvenido al panel de administración de IceBird!
      </CardText>
    </Card>
  </MuiThemeProvider>
)
