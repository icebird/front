import React, { Component } from 'react'
import axios from 'axios'
import ReactLoading from 'react-loading'
import { confirmAlert } from 'react-confirm-alert'
import Alert from 'react-s-alert'
import cookie from 'react-cookies'
import { Redirect } from 'react-router-dom'

import 'react-confirm-alert/src/react-confirm-alert.css'

import Table from '../../Table'
import Actions from '../../Actions/Actions'


export default class Generos extends Component {
  constructor() {
    super()
    this.state = {
      generos: [],
      loaded: false,
      error: false,
      error_message: '',
      reload: false,
    }
  }

  onRemoveClick(item) {
    confirmAlert({
      title: 'Borrar género',
      message: '¿Estás seguro de que quieres eliminar el género: ' + item.nombre + '?',
      buttons: [
        {
          label: 'Sí',
          onClick: () => this.borrarGenero(item),
        },
        {
          label: 'No',
          onClick: () => alert('No'),
        },
      ],
    })
  }

  borrarGenero(item) {
    const config = {
      headers: {'Authorization': 'Bearer ' + cookie.load('token')},
    }
    axios.delete('http://localhost/ICEBIRD/back/public/api/gender/' + item.id, config)
      .then( () => {
        Alert.error('Se ha eliminado la categoria:</br><ul><li>' + item.nombre + '</li></ul>', {
          position: 'top-right',
          effect: 'slide',
          html: true,
        })
        this.setState({ reload: true })
      })
      .catch(error => {
        console.log(error.message)
      })
  }

  listarGeneros(){
    const config = {
      headers: {'Authorization': 'Bearer ' + cookie.load('token')},
    }
    axios.get('http://localhost/ICEBIRD/back/public/api/gender', config)
      .then(res => {
        const generos = res.data
        this.setState({ generos: generos.data, loaded: true })
      })
      .catch(error => {
        console.log(error.message)
        this.setState({ error: true, error_message: error.message, loaded: true })
      })
  }
  componentDidMount() {
    this.listarGeneros()
  }

  render() {
    const noDataText = this.state.error ? this.state.error_message : 'No hay géneros!'
    const link = 'generos/editar/'
    const columns = [
      {
        Header: 'ID',
        accessor: 'id',
      },
      {
        Header: 'Nombre',
        accessor: 'nombre',
      },
      {
        Header: 'Acciones',
        id: 'actions',
        accessor:(item) => (
          <Actions
            item={item}
            linkEdit={link + item.id}
            onRemoveClick={this.onRemoveClick.bind(this)}
          />
        ),

      },
    ]

    if (this.state.reload) {
      return (
        <Redirect to='/admin/dashboard'/>
      )
    }

    if (this.state.loaded === false) {
      return (
        <div className="d-flex justify-content-center mt-5">
          <ReactLoading type="spokes" color="#808080"/>
        </div>
      )
    }

    return (
      <Table
        data={this.state.generos}
        noDataText={noDataText}
        columns={columns}
      />
    )
  }
}
