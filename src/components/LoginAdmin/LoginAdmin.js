import React from 'react'
import { Redirect } from 'react-router-dom'
import ReactLoading from 'react-loading'

import './LoginAdmin.css'

export default class LoginAdmin extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      mail: '',
      pass: '',
    }

    this.onTextChange = this.onTextChange.bind(this)
    this.onClick = this.onClick.bind(this)
  }

  onTextChange(e) {
    this.setState({[e.target.name]: e.target.value})
  }

  onClick() {
    this.props.login(this.state.mail, this.state.pass)
  }

  loading(){
    let loading = ''
    if (this.props.loading) {
      loading = (
        <div className="d-flex justify-content-center mt-5">
          <ReactLoading type="spokes" color="#808080"/>
        </div>
      )
    }
    return loading
  }

  error(){
    let error = ''
    if (this.props.error) {
      error = (
        <div className="alert alert-danger error" role="alert">
          {this.props.error_message}
        </div>
      )
    }
    return error
  }
  render() {
    if (this.props.redirect) {
      return(
        <Redirect to={{
          pathname: '/admin/dashboard',
        }}/>
      )
    }

    return (
      <div className='background'>
        <div className="login-box">
          <div className="login-logo">
            <b>Admin</b>IceBird
          </div>
          <div className="login-box-body">
            <p className="login-box-msg">Inicia sesión para entrar al panel de administración</p>
            <div className="form-group has-feedback">
              <input id="mail" name="mail" className="form-control" onChange={this.onTextChange} placeholder="Email" type="email" />
              <span className="fa fa-envelope form-control-feedback"></span>
            </div>
            <div className="form-group has-feedback">
              <input id="pass" name="pass" className="form-control" onChange={this.onTextChange} placeholder="Contraseña" type="password" />
              <span className="fa fa-lock form-control-feedback"></span>
            </div>
            <button type="button" className="btn btn-primary btn-lg btn-block btn-sm" onClick={this.onClick}>Enviar</button>
          </div>
        </div>
        {this.loading()}
        {this.error()}
      </div>
    )
  }
}
