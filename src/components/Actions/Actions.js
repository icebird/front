import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import SvgIcon from 'react-icons-kit'

import { ic_create } from 'react-icons-kit/md/ic_create'
import { ic_delete } from 'react-icons-kit/md/ic_delete'

export default class Actions extends Component {

  render() {
    return (
      <div className='btn-group'>
        <Link title='Editar' to={this.props.linkEdit} className='btn btn-xs btn-info'>
          <SvgIcon size={20} icon={ic_create}/>
        </Link>
        <button title='Eliminar' type='button' onClick={this.props.onRemoveClick.bind(this, this.props.item)} className='btn btn-xs btn-danger' aria-hidden='true'>
          <SvgIcon size={20} icon={ic_delete}/>
        </button>
      </div>
    )
  }
}
