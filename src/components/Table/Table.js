import React, { Component } from 'react'
import ReactTable from 'react-table'

import 'react-table/react-table.css'
import './Table.css'

export default class Table extends Component {
  render() {
    return (
      <ReactTable
        data={this.props.data}
        noDataText={this.props.noDataText}
        columns={this.props.columns}
        defaultPageSize={10}
        className="-striped -highlight"
      />
    )
  }
}
