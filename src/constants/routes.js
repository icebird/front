import React from 'react'

import Productos from '../components/Admin/Productos'
import Dashboard from '../components/Admin/Dashboard'
import Usuarios from '../components/Admin/Usuarios'
import NuevoProducto from '../components/Admin/NuevoProducto'
import Categorias from '../components/Admin/Categorias/Categorias'
import NuevaCategoria from '../components/Admin/NuevaCategoria/NuevaCategoria'
import Generos from '../components/Admin/Generos/Generos'
import NuevoGenero from '../components/Admin/NuevoGenero/NuevoGenero'
import EditarProducto from '../components/Admin/EditarProducto/EditarProducto'
import EditarCategoria from '../components/Admin/EditarCategoria/EditarCategoria'
import EditarGenero from '../components/Admin/EditarGenero/EditarGenero'
import Pedidos from '../components/Admin/Pedidos/Pedidos'

export const routes = [
  {
    path: '/admin/dashboard',
    main: () => <Dashboard/>,
  },
  {
    path: '/admin/productos',
    exact: true,
    main: () => <Productos/>,
  },
  {
    path: '/admin/usuarios',
    main: () => <Usuarios/>,
  },
  {
    path: '/admin/productos/nuevo',
    main: () => <NuevoProducto/>,
  },
  {
    path: '/admin/productos/editar/:id',
    main: (props) => <EditarProducto {...props}/>,
  },
  {
    path: '/admin/categorias',
    exact: true,
    main: () => <Categorias/>,
  },
  {
    path: '/admin/categorias/nuevo',
    main: () => <NuevaCategoria/>,
  },
  {
    path: '/admin/categorias/editar/:id',
    main: (props) => <EditarCategoria {...props}/>,
  },
  {
    path: '/admin/generos',
    exact: true,
    main: () => <Generos/>,
  },
  {
    path: '/admin/generos/editar/:id',
    main: (props) => <EditarGenero {...props}/>,
  },
  {
    path: '/admin/generos/nuevo',
    main: () => <NuevoGenero/>,
  },
  {
    path: '/admin/pedidos',
    exact: true,
    main: () => <Pedidos/>,
  },
]


