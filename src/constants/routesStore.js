import React from 'react'

import Dashboard from '../components/Admin/Dashboard'

export const routes = [
  {
    path: '/tienda/inicio',
    main: () => <Dashboard/>,
  },
  {
    path: '/tienda/productos',
    exact: true,
    main: () => <h1>Productos</h1>,
  },
]
